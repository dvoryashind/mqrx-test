package com.company.myqrx.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseLongIdEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Table(name = "MQRX_BCARD")
@Entity(name = "mqrx_BCard")
@NamePattern("%s|name")
public class BCard extends BaseLongIdEntity {
    private static final long serialVersionUID = -6166277041510326984L;

    @Composition
    @OneToMany(mappedBy = "bCard", cascade = CascadeType.REMOVE)
    @OrderBy("order")
    private List<Widget> widgets;

    @NotNull
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Widget> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<Widget> widgets) {
        this.widgets = widgets;
    }

}