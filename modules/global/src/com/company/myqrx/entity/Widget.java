package com.company.myqrx.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseLongIdEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "MQRX_WIDGET")
@Entity(name = "mqrx_Widget")
@NamePattern("%s|name")
public class Widget extends BaseLongIdEntity {
    private static final long serialVersionUID = -3893745460528479555L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "ORDER_", nullable = false)
    private Integer order;

    @Lookup(type = LookupType.DROPDOWN, actions = {})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "B_CARD_ID")
    private BCard bCard;

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BCard getbCard() {
        return bCard;
    }

    public void setbCard(BCard bCard) {
        this.bCard = bCard;
    }

}