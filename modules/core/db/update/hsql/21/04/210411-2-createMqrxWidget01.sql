alter table MQRX_WIDGET add constraint FK_MQRX_WIDGET_ON_B_CARD foreign key (B_CARD_ID) references MQRX_BCARD(ID);
create index IDX_MQRX_WIDGET_ON_B_CARD on MQRX_WIDGET (B_CARD_ID);
