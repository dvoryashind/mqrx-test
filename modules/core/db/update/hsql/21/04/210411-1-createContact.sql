create table MYQRX_CONTACT (
    ID bigint not null,
    --
    NAME varchar(255) not null,
    PHONE varchar(15) not null,
    DESCRIPTION longvarchar,
    --
    primary key (ID)
);