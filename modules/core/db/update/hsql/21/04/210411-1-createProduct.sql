create table MYQRX_PRODUCT (
    ID bigint not null,
    UUID varchar(36),
    --
    NAME varchar(255) not null,
    URL varchar(255) not null,
    PRICE double precision not null,
    DESCRIPTION longvarchar,
    --
    primary key (ID)
);