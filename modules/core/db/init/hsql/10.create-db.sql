-- begin MQRX_WIDGET
create table MQRX_WIDGET (
    ID bigint not null,
    --
    NAME varchar(255) not null,
    ORDER_ integer not null,
    B_CARD_ID bigint not null,
    --
    primary key (ID)
)^
-- end MQRX_WIDGET
-- begin MQRX_BCARD
create table MQRX_BCARD (
    ID bigint not null,
    --
    NAME varchar(255) not null,
    --
    primary key (ID)
)^
-- end MQRX_BCARD
-- begin MYQRX_PRODUCT
create table MYQRX_PRODUCT (
    ID bigint not null,
    UUID varchar(36),
    --
    NAME varchar(255) not null,
    URL varchar(255) not null,
    PRICE double precision not null,
    DESCRIPTION longvarchar,
    --
    primary key (ID)
)^
-- end MYQRX_PRODUCT
-- begin MYQRX_CONTACT
create table MYQRX_CONTACT (
    ID bigint not null,
    --
    NAME varchar(255) not null,
    PHONE varchar(15) not null,
    DESCRIPTION longvarchar,
    --
    primary key (ID)
)^
-- end MYQRX_CONTACT
