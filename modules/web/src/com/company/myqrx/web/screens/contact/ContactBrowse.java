package com.company.myqrx.web.screens.contact;

import com.haulmont.cuba.gui.screen.*;
import com.company.myqrx.entity.Contact;

@UiController("myqrx_Contact.browse")
@UiDescriptor("contact-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class ContactBrowse extends MasterDetailScreen<Contact> {
}