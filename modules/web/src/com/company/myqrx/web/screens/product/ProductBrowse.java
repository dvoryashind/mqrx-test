package com.company.myqrx.web.screens.product;

import com.haulmont.cuba.gui.screen.*;
import com.company.myqrx.entity.Product;

@UiController("myqrx_Product.browse")
@UiDescriptor("product-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class ProductBrowse extends MasterDetailScreen<Product> {
}