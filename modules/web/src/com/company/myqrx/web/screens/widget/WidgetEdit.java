package com.company.myqrx.web.screens.widget;

import com.haulmont.cuba.gui.screen.*;
import com.company.myqrx.entity.Widget;

@UiController("mqrx_Widget.edit")
@UiDescriptor("widget-edit.xml")
@EditedEntityContainer("widgetDc")
@LoadDataBeforeShow
public class WidgetEdit extends StandardEditor<Widget> {
}