package com.company.myqrx.web.screens.bcard;

import com.haulmont.cuba.gui.screen.*;
import com.company.myqrx.entity.BCard;

@UiController("mqrx_BCard.browse")
@UiDescriptor("b-card-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class BCardBrowse extends MasterDetailScreen<BCard> {
}