import { ProductManagement } from "./app/product/ProductManagement";
import { ContactManagement } from "./app/contact/ContactManagement";
import { BCardManagement } from "./app/bcard/BCardManagement";
import { getMenuItems } from "@cuba-platform/react-core";

export const menuItems = getMenuItems();

// Code below demonstrates how we can create SubMenu section
// Remove '/*' '*/' comments and restart app to get this block in menu

/*
// This is RouteItem object that we want to see in User Settings sub menu
const backToHomeRouteItem = {
  pathPattern: "/backToHome",
  menuLink: "/",
  component: null,
  caption: "home"
};
// SubMenu object
const userSettingsSubMenu = {
  caption: 'UserSettings', // add router.UserSettings key to src/i18n/en.json for valid caption
  items: [backToHomeRouteItem]};
// Add sub menu item to menu config
menuItems.push(userSettingsSubMenu);
*/

menuItems.push({
  pathPattern: "/bCardManagement/:entityId?",
  menuLink: "/bCardManagement",
  component: BCardManagement,
  caption: "BCardManagement"
});

menuItems.push({
  pathPattern: "/contactManagement/:entityId?",
  menuLink: "/contactManagement",
  component: ContactManagement,
  caption: "ContactManagement"
});

menuItems.push({
  pathPattern: "/productManagement/:entityId?",
  menuLink: "/productManagement",
  component: ProductManagement,
  caption: "ProductManagement"
});
