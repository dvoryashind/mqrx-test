import { BaseLongIdEntity } from "./base/sys$BaseLongIdEntity";
import { Widget } from "./mqrx_Widget";
export class BCard extends BaseLongIdEntity {
  static NAME = "mqrx_BCard";
  widgets?: Widget[] | null;
  name?: string | null;
}
export type BCardViewName = "_base" | "_local" | "_minimal" | "bCard-view";
export type BCardView<V extends BCardViewName> = V extends "_base"
  ? Pick<BCard, "id" | "name">
  : V extends "_local"
  ? Pick<BCard, "id" | "name">
  : V extends "_minimal"
  ? Pick<BCard, "id" | "name">
  : V extends "bCard-view"
  ? Pick<BCard, "id" | "name" | "widgets">
  : never;
