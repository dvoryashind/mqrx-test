import { BaseLongIdEntity } from "./base/sys$BaseLongIdEntity";
export class Contact extends BaseLongIdEntity {
  static NAME = "myqrx_Contact";
  name?: string | null;
  phone?: string | null;
  description?: string | null;
}
export type ContactViewName = "_base" | "_local" | "_minimal";
export type ContactView<V extends ContactViewName> = V extends "_base"
  ? Pick<Contact, "id" | "name" | "phone" | "description">
  : V extends "_local"
  ? Pick<Contact, "id" | "name" | "phone" | "description">
  : V extends "_minimal"
  ? Pick<Contact, "id" | "name">
  : never;
