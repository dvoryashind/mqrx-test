import { BaseLongIdEntity } from "./base/sys$BaseLongIdEntity";
import { BCard } from "./mqrx_BCard";
export class Widget extends BaseLongIdEntity {
  static NAME = "mqrx_Widget";
  name?: string | null;
  order?: number | null;
  bCard?: BCard | null;
}
export type WidgetViewName = "_base" | "_local" | "_minimal";
export type WidgetView<V extends WidgetViewName> = V extends "_base"
  ? Pick<Widget, "id" | "name" | "order">
  : V extends "_local"
  ? Pick<Widget, "id" | "name" | "order">
  : V extends "_minimal"
  ? Pick<Widget, "id" | "name">
  : never;
