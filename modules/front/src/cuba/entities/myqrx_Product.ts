import { BaseLongIdEntity } from "./base/sys$BaseLongIdEntity";
export class Product extends BaseLongIdEntity {
  static NAME = "myqrx_Product";
  uuid?: any | null;
  name?: string | null;
  url?: string | null;
  price?: any | null;
  description?: string | null;
}
export type ProductViewName = "_base" | "_local" | "_minimal";
export type ProductView<V extends ProductViewName> = V extends "_base"
  ? Pick<Product, "id" | "name" | "url" | "price" | "description">
  : V extends "_local"
  ? Pick<Product, "id" | "name" | "url" | "price" | "description">
  : V extends "_minimal"
  ? Pick<Product, "id" | "name">
  : never;
